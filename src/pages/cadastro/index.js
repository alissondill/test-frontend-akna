import React, { Component } from "react";
import { Form, Button, InputGroup, Card, Col, Row } from 'react-bootstrap';
import "./index.css";

export default class Cadastro extends Component {
  constructor() {
    super();

    this.state = {
      books: {
        nome: "",
        autor: "",
        preco: ""
      },
      redirect: false
    }
  }
  render() {
    const { redirect } = this.state;

    if (redirect) {
      return (<Card>
        <Row className="justify-content-md-center">
        Livro cadastrado com sucesso!</Row>
      </Card>);
    } else {


      return (
        <Row className="justify-content-md-center">
          <Col md={6} className="my-2">
            <Card className="formulario" style={{ width: 'auto' }}>
              <Card.Body>
                <Form onSubmit={this.handleSubmit}>
                  <Col md="auto" className="my-1">
                    <Form.Group controlId="">
                      <Form.Label>Nome do Livro</Form.Label>
                      <Form.Control value={this.state.books.nome} onChange={this.handleInputChange} required name="nome" type="name" placeholder="Nome" />
                    </Form.Group>
                  </Col>
                  <Col md={9} className="my-1">
                    <Form.Group controlId="">
                      <Form.Label>Autor do Livro</Form.Label>
                      <Form.Control value={this.state.books.autor} onChange={this.handleInputChange} required name="autor" type="autor" placeholder="Autor" />
                    </Form.Group>
                  </Col>
                  <Col sm={9} className="my-1">
                    <Form.Group controlId="">
                      <Form.Label>Preço do Livro</Form.Label>
                      <InputGroup>
                        <InputGroup.Prepend>
                          <InputGroup.Text id="inputGroupPrepend">R$</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control value={this.state.books.preco} onChange={this.handleInputChange} required name="preco" type="number" step="0.01" placeholder="00,00" />
                      </InputGroup>
                    </Form.Group>
                  </Col>
                  <br />
                  <Button on variant="primary" type="submit">
                    Cadastrar
              </Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      );
    }
  }

  handleInputChange = event => {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState(prevState => ({
      books: { ...prevState.books, [name]: value }
    }))
  }

  handleSubmit = event => {
    fetch("http://localhost:3000/books/", {
      method: "post",
      body: JSON.stringify(this.state.books),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(data => {
        if (data.ok) {
          this.setState({ redirect: true })
        }
      })

    event.preventDefault();
  };
}
