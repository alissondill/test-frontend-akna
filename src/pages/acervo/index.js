import React from "react";
import Listagem from "./listagem";
import { Table, Row, Col} from 'react-bootstrap';
function Acervo() {
  return (
    <Row className="justify-content-md-center">
<Col md={8} className="my-2">
    <Table striped bordered hover>
        <thead>
    <tr>

      <th>Livro</th>
      <th>Autor</th>
      <th>Preço</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <Listagem/>
  </tbody>
      </Table>
      </Col>
      </Row>
  );
}

export default Acervo;
