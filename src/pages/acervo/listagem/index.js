import React, { Component } from "react";
import { Dropdown, Button, Form, Row } from 'react-bootstrap';

export default class Listagem extends Component {
    constructor() {
        super();
        this.state = {
            books: []
            ,
            book: {
                id:"",
                nome:"",
                autor:"",
                preco:""

            },
            edit:false
        }
    }

    componentDidMount() {
        fetch("http://localhost:3000/books")
            .then(books =>
                books.json().then(books => this.setState({ books }))
            )
    }

    render() {
        const { books } = this.state;
        const { edit } = this.state;
        if(edit){
            return (
                <tr>
                    <td><Form.Control name="nome" required onChange={this.handleInputChange} value={this.state.book.nome}></Form.Control></td>
                <td><Form.Control name="autor" required onChange={this.handleInputChange} value={this.state.book.autor}></Form.Control></td>
                <td><Form.Control name="preco" required  onChange={this.handleInputChange} value={this.state.book.preco}></Form.Control></td>
                <td> <Button on variant="success" onClick={this.handleUpdate} type="submit">
                    Save
              </Button></td>
                </tr>
            );

        }else{

        return books.map((book, index) => (

            <tr key={index}>
                <td>{book.nome}</td>
                <td>{book.autor}</td>
                <td>R$ {book.preco}</td>
                <td>
                  <Dropdown >
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Opções
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item id={index} onClick={this.handleEdit}>Editar</Dropdown.Item>
                        <Dropdown.Item id={book.id} onClick={this.handleDelete}>Excluir</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                
                </td>
            </tr>

        ))
        }
    }
    handleDelete = event => {
        const target = event.target;
        const id = target.id;
        fetch("http://localhost:3000/books/" + id, {
            method: "delete",
            body: JSON.stringify(this.state.books),
            headers: {
              "Content-Type": "application/json"
            }
          })
            .then(data => {
              if (data.ok) {
                window.location.reload();
              }
            })
      
          event.preventDefault();
    
        
      }
    handleEdit = event => {
        const target = event.target;
        const editbook = this.state.books[target.id];
        
        this.setState({book: editbook});

        this.setState({ edit: true });
      }

      handleInputChange = event => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
    
        this.setState(prevState => ({
          book: { ...prevState.book, [name]: value }
        }))
      }
      handleUpdate = event =>{
        fetch("http://localhost:3000/books/"+ this.state.book.id, {
            method: "put",
            body: JSON.stringify(this.state.book),
            headers: {
              "Content-Type": "application/json"
            }
          })
            .then(data => {
              if (data.ok) {
                window.location.reload();
              }
            })
      
          event.preventDefault();
      }
}

