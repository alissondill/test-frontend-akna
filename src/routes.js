import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Acervo from "./pages/acervo";
import Cadastro from "./pages/cadastro";

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Acervo} />
        <Route path="/acervo" exact component={Acervo} />
        <Route path="/cadastro" component={Cadastro} />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
