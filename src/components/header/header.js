import React from "react";
import "./header.css";
import { Nav } from 'react-bootstrap';

 const Header = () => (
     <div><header className="main bg-primary"><b>Livraria</b></header>
     <Nav variant="pills" className="justify-content-center bg-warning" >
    <Nav.Item>
      <Nav.Link href="/acervo">Acervo</Nav.Link>
    </Nav.Item>
    <Nav.Item>
    <Nav.Link href="/cadastro" eventKey="link-1">Cadastro</Nav.Link>
    </Nav.Item>
  </Nav>
     </div>
     


 );

 export default Header;